Overview
--------
The adaptive_quiz.module is a framework which allows you to create interactive quizzes 
for your visitors. It allows creation of quizzes with answer conditions and directs the users 
to the appropriate question when answered.


Requirements
------------
Drupal 6.x
PHP 4.3.0 or greater (5 is strongly recommended)
MySQL 5
Quiz Module 4.1 (http://drupal.org/project/quiz)



Installation
------------
1. Enable the module.
2. Create a quiz and enable the "adaptive" option on it.
3. Create a question and select the appropriate "next question" for each response.

Support
-------
- Visit the Quiz group at http://groups.drupal.org/quiz


Credits
-------
- Developer:   Julian Mancera(Colombia)
- Developer:   Michael Favia (US)
- Sponsors:     Worldwide Affinity, Worldclass Dealer Services



